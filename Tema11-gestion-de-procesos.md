<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Tema 11 - Gestión de procesos](#tema-11-gestión-de-procesos)
    - [Objetivos](#objetivos)
    - [¿Qué es un proceso?](#¿qué-es-un-proceso)
        - [Ejercicios 1](#ejercicios-1)
    - [Estados de un proceso](#estados-de-un-proceso)
        - [Ejercicios 2](#ejercicios-2)
    - [Información de un proceso](#información-de-un-proceso)
        - [Ejercicios 3](#ejercicios-3)
    - [Los procesos en un árbol](#los-procesos-en-un-árbol)
    - [Controlando los procesos con kill y killall](#controlando-los-procesos-con-kill-y-killall)
        - [Ejercicios 4](#ejercicios-4)
    - [Prioridades de los procesos con nice y renice](#prioridades-de-los-procesos-con-nice-y-renice)
    - [Otros comandos de gestión de procesos](#otros-comandos-de-gestión-de-procesos)
    - [Comandos vistos en el tema](#comandos-vistos-en-el-tema)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Tema 11 - Gestión de procesos

<!-- START DOCTOC -->
<!-- END DOCTOC -->

## Objetivos

* Entender el concepto de proceso en Linux
* Utilizar los comandos más importantes para obtener información de los procesos.
* Enviar señales a los procesos.
* Modificar la prioridad de los procesos.

## ¿Qué es un proceso?

Un proceso es un «programa en ejecución». Los procesos tienen código que se ejecuta, y datos sobre los que trabaja, pero también algunos atributos:
* El número de proceso (**PID**, «process identity»), que sirve para identificar el proceso, y que sólo puede asignarse a un proceso al mismo tiempo.
* Todos los procesos conocen el número de proceso padre, o **PPID**. Cada proceso puede *engendrar* a otros, que incorporan una referencia a su creador. El único proceso que no tiene *padre*, es el pseudo proceso con PID 0, que es generado durante el arranque del sistema, y que es responsable de crear el proceso «init» con PID igual a 1, que a su vez, se convierte en el antepasado de todos los procesos en el sistema.
* Cada proceso es asignado a un usuario y un conjunto de grupos. Es importante para poder determinar los permisos de acceso que posee a ficheros, dispositivos, etc. Por otro lado, el usuario del proceso puede actuar sobre el mismo, parándolo, finalizándolo, etc. El propietario y el grupo son pasados a los procesos hijo.
* El sistema divide el tiempo de la CPU en pequeños slots («time slices»), que pueden ser del orden de fracciones de segundo. El proceso actual es ejecutado durante uno de esos slots de tiempo, y el sistema debe decidir qué proceso ejecutará en el siguiente intervalo. Esta decisión es responsabilidad del planificador, basándose en la prioridad de los procesos.
* Un proceso tiene otros atributos, como el directorio actual, entorno del proceso,... que también serán enviados a los procesos hijo.
* Podemos consultar el sistema de ficheros `/proc` para encontrar información al respecto. Hay varios directorios que utilizan números como nombres; cada uno de esos directorios corresponde a un proceso y su nombre es el PID del proceso.

En el directorio del proceso, hay varios ficheros que contienen información del proceso. Los detalles se encuentran en la página `man proc(5)`.

> El control de trabajos (jobs) disponible en muchas shells es también un método de gestión de procesos. Un trabajo (job) es un proceso cuyo padre es la shell. Desde la propia shell podemos controlar los trabajos a través de los comandos, `fg`, `bg` y `jobs`, así como con la combinación CTRL-Z y CTRL-C.

### Ejercicios 1

1. ¿Cómo podemos ver las variables de entorno de cualquiera de los procesos?
2. ¿Cuál es el PID máximo que se puede utilizar?

## Estados de un proceso

Otra importante propiedad de un proceso es su estado de proceso. 
* **runnable** - Un proceso que está en memoria, espera a que sea ejecutado por la CPU.
* **operating** - Cuando el proceso está siendo ejecutado por la CPU, y después de agotar su slot de tiempo, vuelve al estado *runnable*.
* **sleeping** - Es frecuente que un proceso tenga que esperar por un dispositivo, y no se le puede asignar tiempo de CPU.
* **stopped** - Procesos parados, por ejemplo, por la combinación CTRL-Z enviada desde la shell.

Una vez que el proceso finaliza, devuelve un código de retorno, que puede ser usado para conocer si se ha ejecutado con éxito o no.

De vez en cuando, los procesos aparecen marcados como zombies usando el estado «Z». Un proceso se convierte en zombie cuando finaliza pero no tiene a quién devolver su código de retorno. Si el proceso *zombie* no desaparece de la tabla de procesos, es porque su padre todavía está esperando el código de retorno. Este proceso *zombie* no puede eliminarse de la tabla de procesos. No hay problema, ya que tampoco está haciendo uso de memoria ni CPU, tan sólo tiene ocupada una entrada de la tabla de procesos.

> Los procesos *zombie* desaparecen cuando su padre también desaparece, ya que que los procesos huérfanos son adoptados por el proceso *init*. El proceso *init* está la mayoría del tiempo esperando los códigos de retorno de los procesos que finalizan, por lo que puede recoger ese código de retorno.

### Ejercicios 2

1. Ejecuta un proceso `xclock` en el segundo plano. En la variable de la shell `$!`, encontramos el PID de ese proceso. Comprueba el estado del proceso a través del comando:<br/>
	`grep ^State: /proc/$!/status`<br/>
   Para el proceso. ¿Cuál es el estado del proceso ahora?
2. Crea un proceso zombie.

## Información de un proceso

Normalmente, no podremos acceder a la información de procesos almacenda en el directorio `/proc`, pero disponemos de comandos para consultarla.	El comando `ps` («process status») está disponible en todo sistema tipo Unix. Sin argumentos, muestra los procesos de la terminal actual. En la lista, se muestran el PID, el terminal TTY, el estado de proceso STAT, el tiempo usado de CPU y el comando que se ha ejecutado.

La sintaxis de ps es un poco confusa, ya que admite el formato de opciones propio de GNU, de BSD y de Unix-98. Algunas de estas opciones son:

* **a** («all») muestra todos los procesos con una terminal
* **--forest** muestra la jerarquía de procesos
* **l** («long») muestra información extra, como la prioridad
* **r** («running») sólo se muestran los procesos «runnable»
* **T** («terminal») muestra todos los procesos de la terminal actual
* **U** <name> («user») muestra los procesos propiedad del usuario 		<name>
* **x** también se muestran los procesos sin terminal.

Si le pasamos al comando ps un PID, sólo muestra cierta información de dicho proceso:

	$ ps 1
Con la opción `-C`, `ps` muestra información sobre el proceso (o procesos), basados en un comando particular:

	$ ps -C bash

### Ejercicios 3

1. El comando `ps` permite que se defina el formato de la salida, a través de la opción `-o`. Estudia la página man ps(1), y escribe el comando que muestra el PID, PPID, estado del proceso y el comando.

## Los procesos en un árbol

Si lo que nos interesa son las relaciones entre los procesos, el comando `pstree` es útil para visualizar un árbol de procesos, en el cual los procesos hijo se muestran como una rama de su proceso padre. Los procesos son mostrados por nombre:

	$ pstree

Los procesos idénticos están agrupados en corchetes y se muestra una cantidad y un asterisco (\*). Las opciones más importantes, son:

* **-p** muestra los PID's junto a los nombres de proceso
* **-u** muestra el nombre del usuario propietario del proceso
* **-G** se muestra con un diseño que usa los caracteres de la terminal gráfica.

## Controlando los procesos con kill y killall

El comando `kill` envía señales a los procesos seleccionados. La señal deseada se especifica bien con un número o con un nombre; también debemos indicar el número del proceso. Algunos ejemplos:

	$ kill -15 4711
	$ kill -TERM 4711
	$ kill -SIGTERM 4711
	$ kill -s TERM 4711
	$ kill -s SIGTERM 4711
	$ kill -s 15 4711

Las señales más importantes, junto con su número y significado:

* **SIGHUP** (1, «hang up») provoca que la shell finalice todos sus procesos hijo que usan la misma terminal que ella. Para procesos en segundo plano sin terminal, se suele usar para volver a leer sus ficheros de configuración.
* **SIGINT** (2, «interrupt») Interrumpe el proceso, equivale a CTRL-C.
* **SIGKILL** (9, «kill») Finaliza el proceso sin poder ignorarse.
* **SIGTERM** (15, «terminate») Valor por defecto para `kill` y `killall`; finaliza el proceso.
* **SIGCONT** (18, «continue») Permite que un proceso parado con SIGSTOP pueda continuar.
* **SIGSTOP** (19, «stop») Para un proceso temporalmente.
* **SIGSTP** (20, «terminal stop») Equivale a la combinación CTRL-Z.

Los números de las señales pueden variar entre diferentes versiones de UNIX, e incluso en Linux. Las señales 1, 9 y 15 se pueden usar sin temor, el resto, mejor usar el nombre.

A menos que se indique otra señal, se enviará la SIGTERM, que finaliza el proceso. Los programas pueden ser escritos de tal forma que capturen estas señales, o bien ignorarlas. Las señales SIGKILL y SIGSTOP no son manejadas por el proceso, sino por el kernel, y por lo tanto no pueden ser capturadas o ignoradas. La señal SIGKILL finaliza el proceso sin darle otra opción al proceso, y la SIGSTOP para el proceso y no se le asigna tiempo de CPU. Existen procesos que se ejecutan en segundo plano, que utilizan la señal SIGHUP para volver a leer sus ficheros de configuración, sin tener que reinciarse.

Por supuesto, sólo podemos aplicar `kill` a aquellos procesos que nos pertenezcan. Hay ocasiones en las que un proceso no reacciona ni siquiera ante una señal SIGKILL, ¿cuál?.	

Una alternativa al comando `kill`, es `killall`. La diferencia radica en que los procesos se designan por el nombre, en lugar del número. Y por lo tanto, se le envía la señal a todos los procesos con ese nombre. También se le envía la señal SIGTERM por defecto.
Las opciones más importantes de killall, son:

* **-i** pide confirmación antes de enviar la señal al proceso.
* **-l** muestra una lista de las señales disponibles.

### Ejercicios 4

1. ¿Qué señales son ignoradas por defecto? Pista: *signal*.
2. Usar el comando `kill` y `killall` para eliminar procesos.

## Prioridades de los procesos con nice y renice

Como sabemos, la CPU debe ser compartida entre varios procesos. Esto es trabajo del planificador de tareas. Y normalmente, hay más de un proceso en ejecución, y el planificador debe asignar el tiempo de CPU según ciertas reglas. Un factor decisivo es la prioridad del proceso.

Como usuarios (o administradores) no podemos configurar las prioridades directamente. Lo que podemos hacer es pedirle al kernel que favorezca o penalice a ciertos procesos. El valor «nice» indica el grado de favoritismo hacia un proceso, y también es enviado a los procesos hijo.
Con el comando `nice` se indica un nuevo valor nice para un proceso.

	nice [-<valor>] <comando> <parámetro> ...

Los posibles valores para `nice` van desde *-20* hasta *+19*. Un valor negativo incrementa la prioridad, y un valor positivo decrementa dicha prioridad. Si no se indica ningún valor, se usa *+10*. Sólo *root* puede iniciar un proceso con valor `nice` negativo. 

La prioridad de un proceso en ejecución puede ser modificada a través del comando `renice`.

De nuevo, sólo el administrador puede asignar valores `nice` arbitrarios. Los usuarios normales sólo pueden incrementar los valores `nice` de sus propios procesos, a través de `renice`.
	
	$ nice -10 sleep 10 &
	$ ps -l
	# nice --15 sleep 10 &
	# ps -l
	# nice -15 sleep 20 &
	# renice 0 'PID'

## Otros comandos de gestión de procesos

Cuando lanzamos un comando usando `nohup`, ese comando ignorará una señal SIGHUP y por lo tanto sobrevivirá a la muerte de su proceso padre.

El comando `top` unifica las funciones de varios comandos de gestión de procesos en un sólo programa. Podemos ver los comandos disponibles, pulsando la tecla *h*. Podemos enviar señales (k), o cambiar el valor nice de un proceso (r).

## Comandos vistos en el tema

Comando | Descripción
-- | --
kill | Envía una seña a un proceso
killall | Envía una señal a todos los procesos con un cierto nombre.
nice | Ejecuta un programa con un valor nice diferente.
nohup |	Inicia un programa que es inmune a la señal SIGHUP.
ps | Muestra información del estado de los procesos.
pstree | Muestra el árbol de procesos.
renice | Cambia el valor nice de un proceso en ejecución.
top | Herramienta orientada a pantalla para monitorizar	procesos.
