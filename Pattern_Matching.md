# Operadores de concordancia de patrones

### ¿Por qué usar operadores de concordancia de patrones?

* El `Pattern Matching` se usa para eliminar _patrones_ del contenido de una variable.
* Es un método para eliminar contenido de una variable. Es decir, mantener aquel contenido con el que nos va interesar trabajar más adelante.
  * Por ejemplo, si la variable `$DATE` contiene `05-01-15`, y sólo deseamos trabajar con el año, debemos eliminar el resto del contenido (día y mes).
  * Si un fichero tiene la extensión `*.odt` y queremos renombrarlo con la extensión `.txt`.

### Explicación con ejemplos

* `${VAR#pattern}` Busca el patrón desde el inicio del valor de la variable `VAR`, eliminando la parte concordante de **menor tamaño** posible, y devuelve el resto.

```
$ FILENAME=/usr/bin/blah
$ echo ${FILENAME#*/}
usr/bin/blah
```

* `${VAR##pattern}` Busca el patrón desde el inicio del valor de la variable `VAR`, eliminando la parte concordante de **mayor tamaño** posible, y devuelve el resto.

```
$ FILENAME=/usr/bin/blah
$ echo ${FILENAME##*/}
blah
```

* `${VAR%pattern}` Si el patrón concuerda con la parte final del valor de la variable `VAR`, elimina la parte de menor tamaño posible, y devuelve el resto.

```
$ FILENAME=/usr/bin/blah
$ echo ${FILENAME%/*}
/usr/bin
```

* `${VAR%%pattern}` Si el patrón concuerda con la parte final del valor de la variable `VAR`, elimina la parte de mayor tamaño posible, y devuelve el resto.

```
$ FILENAME=/usr/bin/blah
$ echo ${FILENAME%%/*}

```

### Ejercicio

Escribir el resultado que mostrarán los siguientes comandos, antes de comprobarlo en una sesión de Bash.

```
BLAH=rababarabarabarara

echo 'El resultado de ##*ba es' ${BLAH##*ba}
echo 'El resultado de #*ba es' ${BLAH#*ba}
echo 'El resultado de %%*ba es' ${BLAH%%ba*}
echo 'El resultado de %ba* es' ${BLAH%ba*}
```
