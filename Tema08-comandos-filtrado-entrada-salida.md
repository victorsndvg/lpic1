# Tema 8 - Comandos de filtrado - Entrada y Salida estándar
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
- [Canales estándar](#canales-estándar)
- [Redirigiendo canales estándar](#redirigiendo-canales-estándar)
    - [Ejercicios 1](#ejercicios-1)
- [Enviar datos entre comandos (pipes)](#enviar-datos-entre-comandos-pipes)
    - [Ejercicios 2](#ejercicios-2)
- [Comandos de filtrado](#comandos-de-filtrado)
    - [Salida y concatenación de ficheros de texto](#salida-y-concatenación-de-ficheros-de-texto)
        - [Ejercicios 3](#ejercicios-3)
    - [Principio y final](#principio-y-final)
        - [Ejercicios 4](#ejercicios-4)
    - [Procesado de texto carácter a carácter](#procesado-de-texto-carácter-a-carácter)
        - [tr](#tr)
        - [expand](#expand)
        - [unexpand](#unexpand)
        - [Ejercicios 5](#ejercicios-5)
    - [Procesado de texto línea a línea](#procesado-de-texto-línea-a-línea)
        - [nl](#nl)
        - [wc](#wc)
        - [Ejercicios 6](#ejercicios-6)
    - [Gestión de datos](#gestión-de-datos)
        - [sort](#sort)
        - [uniq](#uniq)
    - [Columnas y campos](#columnas-y-campos)
        - [cut](#cut)
        - [paste](#paste)
- [Comandos vistos en el tema](#comandos-vistos-en-el-tema)
- [Resumen](#resumen)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivos

* Redirección de los canales de entrada y salida en la shell.
* Conocer los comandos más importantes para operaciones de filtrado.

## Canales estándar

Muchos comandos de Linux están diseñados para leer datos de entrada, manipularlos y mostrar los resultados de esa operación. Por ejemplo:

	$ grep xyz

con este comando podemos escribir líneas de texto con el teclado, y `grep` sólo dejará pasar aquellas que cumplan con el criterio de búsqueda, es decir, aquellas que contengan la secuencia de caracteres `xyz`:

	$ grep xyz
	abd def
	xyz 123
	xyz 123
	aaa bbb
	XXXYYYZZZ
	...

Decimos que `grep` lee los datos desde su «entrada estándar» (el teclado), y los envía hacia su «salida estándar» (la pantalla).
El tercer canal es la «salida de error estándar». Mientras que los resultados los muestra por su salida estándar, `grep` envía hacia la salida de error cualquier mensaje de error.

<p align="center"><img width="600" src="img/T08_01.png"></p>

La shell puede redireccionar eses canales estándar para comandos individuales, sin que los programas noten ningún cambio. La salida estándar puede ser redirigida hacia un fichero, que aunque no exista, será creado. El canal de entrada también puede ser redirigido, y un programa puede recibir su entrada desde un fichero.

## Redirigiendo canales estándar

Podemos redirigir el canal de la salida estándar usando el operador de la shell `>`. En el siguiente ejemplo, la salida del comando `ls -laF` es redirigido hacia el fichero `filelist`:

	$ ls -laF >filelist

Si el fichero `filelist` no existe, será creado. Y si existe un fichero con ese nombre, se sobreescribe su contenido. De hecho, la shell crea dicho fichero aunque el comando contenga un error y no se pueda ejecutar correctamente.

> Si queremos evitar la sobreescritura de ficheros existentes, debemos activar la opción `noclobber` (`set -o noclobber`). En este caso, la redirección hacia un fichero existente, provocará un error.

Podemos visualizar el contenido del fichero `filelist` usando el comando `less`:

	$ less filelist

*¿Hay algo extraño en el contenido del fichero anterior?*

Si lo que queremos es añadir la salida de un comando a un fichero existente sin sustituir su contenido anterior, usaremos el operador `>>`. Si el fichero no existe, también se crea.

	$ date >> filelist
	$ less filelist

Otro método para redirigir la salida estándar de un comando es mediante el uso de los acentos "`". Esto se denomina **sustitución de comando**. La salida estándar del comando entrecomillado será insertada en la línea de comandos. Por ejemplo:

```
$ cat fechas
22/12 Comprar regalos
23/12 Comprar árbol de navidad
24/12 Nochebuena
$ date +%d/%m
23/12
$ grep ^`date +%d/%m` fechas
...
```

> Una sintaxis más adecuada para \`date\` es `$(date)`. Sin embargo, esto sólo está soportado por las shells más modernas.

Podemos usar el operador `<` para redirigir el canal de entrada estándar. Se leerá el contenido del fichero indicado en lugar de la entrada de teclado.

	$ wc -l </etc/passwd
	36

> No hay operador de redirección `<<` para concatenar múltiples ficheros de entrada; para enviar el contenido de varios ficheros como entrada de otro, debemos hacer uso de `cat`
>	$ cat filea fileb filec | wc -l

El operador `|` lo veremos más adelante. La mayoría de los programas aceptan uno o más nombres de fichero como argumentos en la línea de comandos.

> Sin embargo, usamos el operador `<<` para recoger líneas de entrada para un comando hasta que se introduce el comando indicado. Por ejemplo:

```
$ grep Linux <<END
Las rosas son rojas,
las violetas azules,
Linux es bonito,
yo sé que es verdad.
END
```

> Si indicamos la cadena final del *documento* sin comillas, las variables serán evaluadas, y la sustitución de comandos (\`...\` o `$()`) se ejecutarán sobre las líneas del *documento*. Pero con las comillas, el *documento* será procesado tal cual. Compara las salidas:

```
$ cat <<EOF
Hoy es: `date`
EOF

$ cat <<"EOF"
Hoy es: `date`
EOF
```

Por supuesto, la entrada y salida estándar pueden redireccionarse al mismo tiempo. La salida del contador de palabras se escribe en un fichero llamado `wordcount`:

```
$ wc -w < frog.txt > wordcount
$ cat wordcount
```

Además de los canales de entrada y salida estándar, también disponemos del canal de salida estándar. Si ocurre algún error durante la ejecución del comando, los mensajes de error se envían hacia dicha salida. De esta forma, se verán los mensajes de error aunque hayamos redireccionado la salida estándar.
Si queremos redireccionar la salida de error debemos indicarlo con el número de canal (2), algo que no era necesario para las salidas anteriores (0 y 1).
Usamos el operador `>&` para redireccionar un canal hacia otro:

	$ make >make.log 2>&1

> El orden es muy importante !
	make >make.log 2>&1
	make 2>&1 >make.log

### Ejercicios 1

1. Podemos usar la opción `-U` para obtener la salida del comando `ls` sin ordenar. Por lo tanto, después de ejecutar `ls -laU >filelist`, la longitud del fichero `filelist` en la salida del comando `ls` es cero. Explicar el motivo.
2. Compara la salida de los comandos `ls /tmp` y `ls /tmp >ls-tmp.txt` ¿Qué se observa? ¿Cómo se explica?
3. ¿Por qué no es posible reemplazar un fichero con una nueva versión en un sólo paso, por ejemplo, `grep xyz file >file`?
4. Y, ¿qué error hay en `cat foo >>foo`, asumiendo que `foo` es un fichero no vacío?
5. En la shell, ¿cómo puedo crear un mensaje de error y visualizarlo a través de la salida de error estándar?

## Enviar datos entre comandos (pipes)

La redirección de la salida suele ser usada para almacenar el resultado de un programa y continuar el proceso con un comando diferente. Sin embargo, este tipo de intercambio puede resultar tedioso, además de tener que deshacerse más tarde de aquellos ficheros que ya han sido utilizados.
Por supuesto, Linux ofrece un método para unir comandos directamente a través de conexiones, llamadas *tuberías*, de tal forma que la salida de un programa se convierte en la entrada de otro.

	$ ls -la | less

Las *tuberías* entre comandos se pueden encadenar entre varios comandos, donde incluso el resultado final se envía hacia un fichero:

	$ cut -d: -f1 /etc/passwd | sort | pr -2 >userlist

Esta línea extrae todos los nombres de usuario presentes en la primera columna del fichero `/etc/passwd`, los ordena alfabéticamente y los escribe en el fichero `userlist` en un formato de dos columnas.
A veces puede resultar útil almacenar un flujo de datos en el interior de una *tubería* en un punto intermedio, por ejemplo, porque el resultado intermedio en esa etapa es útil para diferentes tareas. El comando `tee` copia los datos y los envía a la salida estándar y a un fichero de forma simultánea.

<p align="center"><img width="600" src="img/T08_01.png"></p>

El comando `tee` sin opciones crea el fichero indicado, o lo sobreescribe si existe; con la opción `-a` (append), la salida puede añadirse a un fichero existente:

	$ ls -laF | tee lista | less

En este ejemplo, el contenido del directorio actual se escribe tanto al fichero `lista` como en la pantalla.

> El fichero `lista` no se muestra en la salida de `ls`, porque es creado posteriormente por el comando `tee`.

### Ejercicios 2

1. ¿Cómo puedo escribir el mismo resultado intermedio a varios ficheros al mismo tiempo?

## Comandos de filtrado

Uno de los principios básicos de UNIX (y por lo tanto de Linux) es el concepto de *kit de herramientas*. El sistema incorpora un gran número de programas del sistema, donde cada uno de ellos ejecuta una tarea simple (al menos, conceptualmente hablando). Esos programas se pueden usar como bloques de construcción para a su vez construir otros programas, y ahorrar a los autores el desarrollo de las funciones requeridas. Por ejemplo, no todos los programas incorporan su propia rutina de ordenamiento, ya que muchos de ellos utilizan el comando `sort` proporcionado por Linux.

Esta estructura modular tiene varias ventajas:

* Alivia el trabajo a los programadores, que no necesitan desarrollar (o añadir) nuevas rutinas de ordenamiento cada vez que escriben un programa.
* Si se soluciona algún *bug* en el comando `sort`, o se le añade alguna función adicional, todos los programas que lo usan se benefician de ella, además de que en la mayoría de los casos no es necesario realizar ningún cambio en los programas que hacen uso del comando.

Las herramientas que toman su entrada de la entrada estándar y escriben su salida a la salida estándar, son denominados *comandos de filtrado*, o *filtros*. Si no hay redirecciones, un filtro leerá su entrada desde el teclado. Para finalizar la entrada desde el teclado, se debe usar la secuencia `CTRL-D`, que se interpreta como `EOF` (fin de fichero) por el driver del terminal.

### Salida y concatenación de ficheros de texto

El comando `cat` (con**cat**enar), en realidad está diseñado para unir varios ficheros en uno sólo. Si le pasamos tan sólo un nombre de fichero, se mostrará el contenido de ese fichero por la salida estándar. Y si no le pasamos ningún parámetro, `cat` lee su entrada estándar, y esto aunque pueda parecer de poca utilidad, `cat` ofrece opciones para numerar líneas, añadir finales de línea, hacer visibles caracteres especiales, o comprimir grupos de líneas en blanco en una sola.

> En este momento debemos resaltar que sólo los ficheros de texto tienen un buen comportamiento en pantalla cuando se usan con `cat`. Si se aplica el comando a otro tipo de ficheros (por ejemplo: `/bin/cat`), es más que probable -al menos en una terminal de texto- que el prompt de la shell se transforme en un montón de caracteres no legibles. Si esto sucede, se puede restaurar el conjunto normal de caracteres usando el comando `reset`. Por supuesto, si se redirige la salida hacia un fichero no tendremos este problema.

> Es habitual que se haga un uso abusivo del comando `cat`. En la mayoría de los casos, los comandos utilizados pueden leer de ficheros y no tan sólo lo hacen de su entrada estándar, por lo tanto no es necesario usar `cat` para pasar un fichero a la entrada estándar. Un comando como `cat data.txt | grep foo` no es necesario si se puede escribir de forma más sencilla: `grep foo data.txt`. Incluso aunque `grep` sólo fuera capaz de leer de su entrada estándar, `grep foo <data.txt` sería más corto y no implicaría el uso del comando `cat`.

El comando `tac`, es un *cat hacia atrás*, y hace el mismo trabajo que éste: lee una serie de ficheros, o su entrada estándar, y muestra las líneas que ha leído, pero en orden inverso. Sin embargo, es hasta aquí donde llegan las semejanzas: `tac` no soporta las mismas opciones que `cat`, e incorpora las suyas propias. Por ejemplo, se puede usar la opción `-s` para configurar un separador alternativo que el programa usará para revertir la entrada -normalmente el separador es el carácter de nueva línea, por lo que la salida es dada la vuelta línea a línea.

	$ echo A:B:C:D | tac -s :

La salida anterior puede parecer un poco extraña, pero puede ser explicada de la siguiente manera:
La entrada está formada por cuatro partes `A:`, `B:`, `C:` y `D\n` (el separadar `:`, se entiende que pertenece a la parte que le precede, y el último carácter de nueva línea lo ha insertado el comando `echo`. Estas cuatro partes se colocan en sentido contrario, `D\n` es el primero y luego van las otras tres, ya que cada una de ellas posee un carácter de separación. De esta forma el prompt de la shell queda junto a la salida. Si usamos la opción `-b`, se considera que el separador pertenece a la parte que le sigue en lugar de la que le precede.

#### Ejercicios 3

1. ¿Cómo se puede comprobar si un directorio contiene ficheros con nombres *extraños*, como espacios al final o caracteres de control en el nombre?

### Principio y final

Hay situaciones en las que sólo nos interesa una parte del fichero. Las primeras líneas para comprobar que es el fichero correcto, o en particular con ficheros de *log*, las últimas entradas.	Los comandos `head` y `tail` ejecutan exactamente esto -por defecto las 10 primeras (y últimas) líneas de cada uno de los ficheros pasados como argumentos (o incluso con la entrada estándar).	La opción `-n` le permite especificar un número diferente de líneas; `head -n 20` devuelve las 20 primeras líneas de su entrada estándar, y `tail -n 5 data.txt`, las cinco últimas del fichero `data.txt`.

> La tradición dice que se puede especificar el número *n* de líneas deseadas, directamente con `-n`. Oficialmente, esto no es admitido, pero las versiones Linux de `head` y `tail` sí lo soportan.

Se puede usar la opción `-c` para especificar que la cuenta debe hacerse en bytes, no en líneas: `head -c 20` muestra los primeros 20 bytes, sin importar cuántas líneas ocupen.

	$ head -c 20 /etc/passwd

El comando `head` soporta el símbolo menos: `head -c -20` mostrará todo menos los últimos 20 bytes.

	$ head -c -20 /etc/passwd

> A modo de venganza, `tail` puede hacer algo que `head` no soporta. Si el número de líneas comienza con *+*, muestra todo empezando en la línea indicada: `$ tail -n +3 file`

El comando `tail` también soporta la opción `-f`. Esto provoca que `tail` espere aún después de haber encontrado el fin de fichero, para mostrar los datos que se pueden ir añadiendo más tarde. Esto es muy útil para echarles un vistazo en tiempo real a los ficheros de *log*. Si le pasamos varios ficheros a `tail -f`, coloca una línea de cabecera enfrente de cada bloque de salida.

#### Ejercicios 4

1. ¿Cómo puedo obtener tan sólo la línea número 13 de la entrada estándar, o de un fichero?
2. Crea un fichero y ejecuta `tail -f` sobre el mismo. Ahora, desde otra pestaña, o consola virtual, añade algo a dicho fichero usando el operador `>>`, y observa la salida de `tail`.
3. Observa la salida de `tail` cuando le pasamos varios ficheros.
4. ¿Qué le ocurre a `tail -f` si el fichero observado se trunca?
5. Explica la salida de los siguientes comandos:<br/> 
`$ echo Hola >/tmp/hola`<br/>
`$ echo Hello world >/tmp/hola`<br/>
   cuando tenemos ejecutando el comando:<br/>
`$ tail -f /tmp/hola`<br/>
   en una ventana diferente después del primer `echo`.

### Procesado de texto carácter a carácter

#### tr

El comando `tr` es usado para reemplazar caracteres simples por otros diferentes en un texto, o para eliminarlos, `tr` es un comando de filtro en el sentido más estricto, no toma como argumentos los nombres de los ficheros, sino que trabaja con los canales estándar. Para sustituciones, la sintaxis es:

	tr <s1> <s2>

Los dos parámetros son cadenas de caracteres describiendo la sustitución. En el caso más simple, el primer carácter en «s1» será sustituido por el primer carácter en «s2», el segundo de «s1» por el segundo de «s2», y así sucesivamente.

Si «s1» tiene mayor longitud que «s2», los caracteres de exceso de «s1» son sustituidos por el último carácter de «s2»; y si «s2» es más largo que «s1», los caracteres extra de «s2» son ignorados. Por ejemplo:

	tr Aeiu aey <ejemplo.txt >nuevo.txt

Este comando lee el fichero `ejemplo.txt` y reemplaza las “A” por “a”, las “E” por “e”, y todas las “i” y “u”, por “y”. El resultado se almacena en `nuevo.txt`. Es posible expresar secuencias de caracteres por rangos, de la forma «m-n», donde «m» debe preceder a «n» en el orden del alfabeto.

Con la opción `-c`, `tr` no reemplaza el contenido de «s1», sino su complementario, es decir, todos los caracteres que NO están en «s1». Por ejemplo:

	tr -c A-Za-z ' ' <ejemplo.txt >nuevo.txt

reemplaza todas las «no-letras» del fichero `ejemplo.txt`, por espacios. Para eliminar caracteres, sólo se necesita especificar «s1», el comando:

	tr -d a-z <ejemplo.txt >nuevo.txt

elimina todas las letras minúsculas de `ejemplo.txt`.

Se pueden reemplazar varias ocurrencias seguidas de un carácter por uno solo:

	tr -s '\n' <ejemplo.txt >nuevo.txt

elimina las secuencias de líneas vacías, mediante el reemplazamiento de caracteres de nueva línea por uno sólo.	La opción `-s` (squeeze, “exprimir”) también hace posible sustituir dos caracteres de entrada diferentes por dos idénticos, y luego reemplazarlos por uno solo (al igual que hacía la opción `-s` con un sólo argumento):

	tr -s AE X <ejemplo.txt >nuevo.txt

convierte todos los caracteres «A» y «E» (y las secuencias de ellos) en una «X».

#### expand

El tabulador es una excelente herramienta para insertar sangrías cuando programamos. Por defecto, la marca del tabulador está configurada en un número determinado de columnas (cada 8 columnas, 8, 16, 24...), y la mayoría de editores se mueven a la siguiente parada a la derecha cuando pulsamos la tecla TAB. Como resultado de esto, algunos programas escriben los caracteres de tabulador tal cual, y otros programas pueden no interpretar esto de forma correcta.
	El comando `expand` está aquí para ayudarnos. Lee los ficheros pasados como parámetros (o la entrada estándar) y los envía hacia su salida estándar con los «tabs» eliminados y en su lugar sitúa el número de espacios necesarios para mantener las marcas de tabulador cada 8 columnas.

Con la opción `-t`, podemos indicar un factor de escalado diferente para las marcas de tabulador; un valor típico es `-t 4`. Si le pasamos varios números separados por comas con la opción `-t`, las marcas de tabulador serán colocadas exactamente en esas posiciones. Los tabuladores adicionales de una línea de entrada serán reemplazados por espacios. La opción `-i` (initial) provoca que sólo las tabulaciones situadas al principio de la línea sean reemplazadas por espacios.

#### unexpand

El comando `unexpand`, en cierta medida, deshace el efecto de `expand`. Todas las tabulaciones y espacios en el principio de las líneas de entrada, son reemplazadas por la secuencia más corta de tabuladores y espacios, dando como resultado la misma sangría. Una línea que comience con 1 tabulador, dos espacios, otro tabulador y nueve espacios, asumiendo las marcas de tabulador cada 8 columnas, serán reemplazadas por una línea que comienza con tres tabuladores, y un espacio. La opción `-a` (all) optimizará las secuencias de tabuladores y espacios situadas más allá del principio de la línea.

#### Ejercicios 5

1. El famoso general romano Julio César hacía uso del siguiente sistema de cifrado para transmitir mensajes secretos:<br/>
	*La letra «A» era reemplazada por «D», «B» por «E» y así sucesivamente; «X» por «A», «Y» por «B» y «Z» por «C».*<br/>
	¿Qué comando tr usarías para cifrar los mensajes? ¿y para descifrarlos?
2. ¿Qué comando `tr` usarías para reemplazar todas las vocales de la siguiente frase por una sola?<br/>
	WITH FREE SOFTWARE YOU HAVE FREEDOM
3. ¿Cómo transformarías un fichero de texto de tal forma que toda la 'puntuación' fuera eliminada, y cada palabra apareciera separada en cada una de las líneas?
4. Escribe un comando `tr` para eliminar los caracteres «-» «a» y «z» de la entrada estándar.
5. ¿Cómo podríamos convencernos a nosotros mismos de que realmente `unexpand` reemplaza espacios y tabuladores por una secuencia óptima?

### Procesado de texto línea a línea

#### nl

El comando `nl` se utiliza para numerar las líneas. Si no se especifica nada más, se enumeran las líneas de su entrada de forma consecutiva (las líneas con contenido):

	$ nl /etc/passwd

Esto también lo podría realizar el comando `cat -b`. Aunque `nl` permite un mayor control sobre el proceso de numeración de las líneas:

	$ nl -b a -n rz -w 5 -v 1000 -i 10 frog.txt

La opción `-b a`, causa que todas las líneas sean numeradas, no sólo las «no vacías». `-n rz` formatea los números de línea rellenando con ceros por la derecha. `-w 5`, crea una columna de 5 posiciones. `-v 1000` comienza la numeración en 1000, y `-i 10` incrementa el número de línea de 10 en 10 por cada línea.

Además, `nl` también puede manejar números de línea por página. Esto se organiza usando las cadenas «mágicas» `\:\:\:` o `\:`, como se muestra en el ejemplo:

	$ cat nl-test.txt

Cada página tiene una cabecera y un pie de página, así como un cuerpo conteniendo el texto. La cabecera se introduce usando `\:\:\:`, y separado del cuerpo usando `\:\:`. El cuerpo finaliza con `\:`. Por defecto, `nl` numera las líneas de cada una de las páginas empezando en 1; la cabecera y el pie de página no se numeran.

#### wc

El nombre del comando `wc` es una abreviatura de «word count». Y no sólo sirve para contar palabras, también cuenta los caracteres y las líneas de entrada (ficheros y entrada estándar). Desde el punto de vista de `wc`, una «palabra» es una secuencia de una o más letras. Sin ninguna opción, se muestran los tres valores correspondientes a líneas, palabras y caracteres.

	$ wc frog.txt

y con alguna de las opciones activadas se pueden limitar los resultados:

	$ ls | wc -l

#### Ejercicios 6

1. Numera las líneas del fichero `frog.txt` con un incremento de 2 unidades por línea, comenzando en 100.
2. ¿Cómo puedo numerar las líneas de un fichero en orden inverso?
3. ¿En qué se diferencian las salidas de<br/>
	`wc a.txt b.txt c.txt`<br/>
	y de<br/>
	`cat a.txt b.txt c.txt | wc`?

### Gestión de datos

#### sort

El comando `sort` permite ordenar las líneas de ficheros de texto según un criterio. Por defecto, es por orden alfabético ascendente (A a la Z). Se pueden ordenar no sólo teniendo en cuenta toda la línea, sino sólo una determinada columna o campo de una tabla. Los campos son numerados empezando en 1. Con la opción `-k 2`, el primer campo no se tiene en cuenta, y es el segundo campo el que se ordena. Si hay valores iguales en este campo, se sigue comparando con los siguientes campos, a no ser que se indique un campo de fin `-k 2,3`.
El caracter espacio sirve como separador de campos. Y si hay varios espacios seguidos, tan solo se toma el primero como separador, y el resto pertenecen al campo.

	$ sort -k 2,2 participantes.dat
	$ sort -r -k 2,2 participantes.dat

Con la opción `-t` podemos seleccionar un caracter arbitrario en lugar del separador espacio. Esto es una buena idea, debido a que los campos pueden contener espacios. La nueva versión `participantes0.dat` es menos legible, pero más manejable.

Ahora es más fácil ordenar, por ejemplo por número de participante:

	$ sort -t -k4 participantes0.dat

Por supuesto, la ordenación se realiza con criterios de alfabeto. Esto se puede arreglar con la opción `-n`:

	$ sort -t: -k4 -n participantes0.dat

#### uniq

El comando `uniq` hace el importante trabajo de dejar pasar sólo la primera de una secuencia de líneas iguales en la entrada (o la última, como prefiera!). Lo que se considera como igual, puede ser detallado en las opciones. Sólo admite un fichero de entrada, y si se indica otro, será donde se almacene el resultado. `uniq` trabaja mucho mejor si las líneas de entrada están ordenadas, por lo que todas las líneas iguales estarán unas detrás de otras. Si no es el caso, no está garantizado que cada línea se muestre una sola vez en la salida.

	$ cat uniq-test
	$ uniq uniq-test
	$ sort -u uniq-test

### Columnas y campos

#### cut

Mientras que puedes localizar y extraer líneas de un texto usando `grep`, el comando `cut` trabaja a través de un fichero de texto «por columna». Y lo hace de dos formas:

1. Una posibilidad es el tratamiento absoluto de las columnas. Esas columnas corresponden a caracteres simples en una línea. Para extraer tales columnas, el número de columna debe ser indicado después de la opción `-c`. Para cortar varias columnas en un sólo paso, se pueden especificar como una lista separada por comas. Incluso se pueden indicar rangos de columnas.

	```
	$ cut -c 12,1-5 participantes.dat
	```

	En el ejemplo anterior, se extraen la primera letra del nombre propio y las cinco letras del apellido. La salida contiene las columnas en el mismo orden que en la entrada, incluso si los rangos se solapan, cada carácter de entrada se muestra una sóla vez.<br/>

	```
	$ cut -c 1-2,2-6,2-7 participantes.dat
	```

2. Y el segundo método es extraer campos relativos, los cuales son delimitados por separadores. Debemos usar la opción `-f` y el número de campo deseado. Se aplican las mismas normas que antes. El uso de estas opciones es mutuamente exclusivo. El separador por defecto es tabulador, hay que especificar el uso de uno distinto:

	```
	$ cut -d: -f 1,4 participantes0.dat
	```

#### paste

El comando `paste` une las líneas de los ficheros especificados. Se suele utilizar junto a `cut`. No es un comando de filtrado. Con un símbolo «-» lee de su entrada estándar. Su salida se envía siempre a la salida estándar.	Este comando trabaja por líneas, si se le pasan dos ficheros, la primera línea del primer fichero se une con la primera línea del segundo fichero, usando un tabulador como separador. Para indicar un separador diferente, usamos la opción `-d`.

	$ cut -d: -f4 participantes0.dat >numero.dat
	$ cut -d: -f1-3,5 participantes0.dat | paste -d: numero.dat - >p-numero.dat
	$ cat p-numero.dat

Ahora el fichero puede ser ordenado por el número de participante.

Con la opción `-s`, los ficheros son procesados en secuencia. Primero todas las líneas del primer fichero son unidas en una sola línea con el tabulador en medio, luego todas las del segundo fichero en la segunda línea, etc.

	$ cat lista1
	$ cat lista2
	$ paste -s lista*

## Comandos vistos en el tema

Comando | Descripción
-- | --
cat | Concatena ficheros (aparte de hacer otras cosas ;-) )
cut | Extrae campos o columnas de su entrada
expand | Reemplaza tabuladores por espacios equivalentes
head | Muestra el principio de un fichero
paste | Une líneas de diferentes ficheros de entrada
reset | Resetea el juego de caracteres de la terminal
sort | Ordena su entrada por línea
tac | Muestra un fichero de fin a principio
tail | Muestra el final de un fichero
uniq | Reemplaza secuencias de líneas idénticas por una sola
wc | Cuenta los caracteres, palabras y líneas desde su entrada

## Resumen

* `wc` puede usarse para contar las líneas, palabras y caracteres de la entrada estándar.
* `sort` es un programa versátil para ordenar.
* El comando `cut`, corta rangos específicos de columnas o campos de cada línea de su entrada.
* Con `paste`, se pueden unir las líneas de los ficheros.
