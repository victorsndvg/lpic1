# Tema 18 - Gestión de paquetes con RPM y YUM

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
- [Introducción](#introducción)
- [Gestión de paquetes usando rpm](#gestión-de-paquetes-usando-rpm)
    - [Instalación y actualización](#instalación-y-actualización)
    - [Desinstalación](#desinstalación)
    - [Consultas a la base de datos](#consultas-a-la-base-de-datos)
    - [Verificación de paquetes](#verificación-de-paquetes)
    - [El programa rpm2cpio](#el-programa-rpm2cpio)
- [YUM](#yum)
    - [Repositorios de paquetes](#repositorios-de-paquetes)
    - [Instalar y desinstalar paquetes usando YUM](#instalar-y-desinstalar-paquetes-usando-yum)
    - [Información sobre paquetes](#información-sobre-paquetes)
    - [Descarga de paquetes](#descarga-de-paquetes)
- [Comandos vistos en el tema](#comandos-vistos-en-el-tema)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivos

* Conocer los conceptos básicos de RPM.
* Ser capaz de usar *rpm* para la gestión de paquetes.
* Ser capaz de usar *YUM*.

## Introducción

El «Red Hat Package Manager»(RPM) es una herramienta de gestión de paquetes software. Soporta la instalación y desinstalación de paquetes, mientras que se asegura que diferentes paquetes no entren en conflicto, y que las dependencias entre ellos son tenidas en cuenta. Además, permite realizar consultas sobre los paquetes y asegurarse de la integridad de los mismos.	El núcleo de RPM es una base de datos. Se añaden a la base cuando son instalados y se eliminan cuando son desinstalados. El formato RPM es usado por varias distribuciones (desde la propia Red Hat, Novell/SUSE, TurboLinux, Mandriva, Fedora...).

Y un paquete *rpm* cualesquiera no puede ser instalado en todas las distribuciones. Dicho paquete contiene software compilado, y puede haber variaciones entre las diferentes distribuciones. RPM fue desarrollado originalmente por Red Hat, y se llamó «Red Hat Package Manager», pero luego fue usado por otras distribuciones, por lo que se rebautizó «RPM Package Manager». Un paquete RPM tiene un nombre compuesto, por ejemplo:

	openssh-3.5p1-107.i586.rpm

que consiste en el nombre del paquete (openssh-3.5p1-107), la arquitectura (i586) y el sufijo «.rpm». El nombre del paquete se usa para identificar internamente el paquete una vez instalado. Y está formado por el nombre del software (openssh), la versión asignada por los desarroladores originales (3.5p1), seguido del número de liberación (107) asignada por el constructor del paquete (el distribuidor).

El «RPM Package Manager» es invocado usando el comando `rpm`, seguido por un modo básico. Los modos más importantes se verán más adelante, pero aquellos más complejos, como la inicialización de la base de datos, o la construcción y firma de paquetes, están fuera del alcance de este curso.

## Gestión de paquetes usando rpm

Instalación y actualización. Un paquete RPM se instala con el modo `-i`, seguido de la ruta del fichero del paquete, por ejemplo:

	# rpm -i /tmp/aircrack-ng-1.0-2.fc12.i686.rpm

También podemos indicar la ruta a una dirección HTTP o FTP, para instalar paquetes desde servidores remotos. Está permitido especificar varios paquetes al mismo tiempo:

	# rpm -i /tmp/*.rpm

Adicionalmente, hay dos modos relacionados, que son `-U` (upgrade) y `-F` (freshen). La primera elimina cualquier versión anterior de un paquete e instala la nueva versión, mientras que el segundo instala el paquete SÓLO si existe una versión anterior instalada (que será eliminada).

### Instalación y actualización

Después de estas opciones, podemos indicar que se muestre una barra de progreso (`-h` «hash mark»). Otra opción es `--test`, que no realiza la instalación, sino que tan solo comprueba posibles conflictos (debe utilizarse junto con `-i`). Cuando ocurre un conflicto, el paquete no se instala. Algunos de estos conflictos son:

* Ya existe ese paquete instalado.
* Se requiere realizar la instalación de un paquete ya instalado en una versión diferente (-i), o existe una versión más actual (-U).
* La instalación pretende escribir un fichero que pertenece a un paquete diferente.
* Un paquete necesita un paquete diferente que no está instalado, o que está a punto de ser instalado.

### Desinstalación

Los paquetes pueden ser desinstalados usando el modo `-e` («erase»):

	# rpm -e hello

Debemos indicar el nombre del paquete en lugar del nombre fichero, ya que RPM no recuerda éste último. También podemos abreviar el nombre del paquete si es el único existente. También podemos hacer uso de las opciones `--test` y `--nodeps`. En principio, se eliminan todos los ficheros del paquete, a no ser que existan ficheros de configuración que hayan sido modificados, que permanecerán con la extensión `.rpmsave`.

### Consultas a la base de datos

El modo para obtener información acerca de los paquetes es `-q` (query).

	$ rpm -q openssh
	$ rpm -q aircrack-ng

Se debe indicar un nombre, que si es abreviado, nos responde con el nombre completo del paquete. Puede utilizarse para conocer la versión instalada.

	$ rpm -qf /usr/bin/ssh

De este modo, sabemos a qué paquete pertenece un determinado fichero.

	$ rpm -qa

Listamos todos los paquetes instalados.

	$ rpm -qa | grep cups

Finalmente, podemos consultar por paquetes no instalados. Usamos la opción `-p`, seguido del nombre del fichero.

	$ rpm -qp /tmp/openssh-5.3p1-19.fc12.i686

El resultado no parece muy espectacular, debido a que nos muestra el mismo nombre de paquete que ya aparece en el nombre del fichero. Pero, nos sirve para verificar que el nombre del fichero no ha sido modificado. Si no estamos interesados en el nombre, podemos ampliar la información con la opción `-i`.

	$ rpm -qi openssh

y con la opción `-l`, obtenemos la lista de los ficheros pertenecientes al paquete:

	$ rpm -ql openssh

y de una forma más ampliada:

	$ rpm -qlv openssh

Es importante destacar, que este listado de ficheros incluye todos aquellos que fueron copiados durante la instalación del paquete, no aquellos que se pudieron generar a posteriori, mediante scripts de instalación del paquete, o durante su funcionamiento (ficheros de log).

### Verificación de paquetes

Cuando descargamos un paquete debemos tener en cuenta que la descarga se pudo realizar de forma incorrecta, o incluso el paquete estar falsificado (troyano). RPM nos protege de ambos escenarios, mediante la siguiente opción:

	$ rpm --checksig /tmp/aircrack-ng-1.0-2.fc12.i686.rpm

Se comprueba el checksum MD5 que el paquete contiene en su interior, además su firma interna (SHA1) es comparada con la clave pública del gestor del paquete. Por supuesto, debemos tener la clave pública del distribuidor en nuestro sistema.

En cuanto a la verificación posterior.

	# rpm -V openssh-clients

compara ciertos valores de la base de datos con los ficheros del sistema. Aunque tampoco nos proporciona seguridad, ya que un atacante podría modificar tanto el fichero como la base de datos, pero nos puede servir para detectar errores en ficheros del sistema provocados, por ejemplo, por un corte de luz.

### El programa rpm2cpio

Los paquetes RPM son básicamente archivos *cpio* con una cabecera. Podemos, de este modo, extraer los ficheros individuales de un paquete RPM sin tener que instalar el paquete primero. Simplemente, convertimos el paquete RPM a un archivo *cpio* usando el programa `rpm2cpio`, y lo enviamos hacia `cpio`. Para ello, utilizaremos una tubería.

	$ rpm2cpio hello-2.5-1.fc12.i686.rpm | cpio -t
	$ rpm2cpio hello-2.5-1.fc12.i686.rpm \
		| cpio -idv ./usr/share/man/man1/hello.1.gz

	$ zcat usr/share/man/man1/hello.1.gz | head

## YUM

El programa `rpm` tiene límites. Por ejemplo, no facilita la búsqueda de paquetes que se pueden instalar, hay que indicarle exactamente el paquete a instalar. **YUM** (Yellow Dog Update, Modified) permite el acceso a repositorios disponibles tanto en Internet com en CD-ROM (es similar a un apt-get). Dispone incluso de una shell (yum shell).

Comando | Descripción
-- | --
install | Instala un paquete (o varios) a través del nombre del paquete. También instala las dependencias de los paquetes indicados.
update | Actualiza el paquete indicado a la última versión disponible. Si no se indica ningún paquete concreto, actualiza todos los paquetes instalados en el sistema.
check-update | Comprueba las actualizaciones disponibles. Si hay alguna disponible, se muestra el nombre, version y repositorio.
upgrade | Opera igual que *update* con la opción *--obsoletes* activada, por lo que actualizará la versión de la distribución.
remove/erase | Elimina un paquete del sistema y sus dependencias.
list | Muestra información relativa a un paquete.
provides/whatprovides | Muestra la información del paquete que proporciona un determinado programa o característica.
search | Buscar nombres de paquetes, resúmenes y descripciones para una determinada palabra clave. Lo podemos usar si no conocemos el nombre del paquete.
info | Vacía el directorio cache usado por *yum*.
shell | Inicia el modo shell de Yum.
resolvedep | Muestra los paquetes que concuerdan con la dependencia indicada.
localinstall | Instala los ficheros RPM locales, haciendo uso de los repositorios para resolver las dependencias.
localupdate | Actualiza el sistema haciendo uso de los ficheros RPM locales.
deplist | Muestra las dependencias de un determinado paquete.

### Repositorios de paquetes

Un repositorio es un conjunto de paquetes que están disponibles a través de la red, y que permite la instalación de los mismos a través de Yum.

	$ yum repolist

muestra una lista de los repositorios configurados.

	$ yum repolist disabled

muestra una lista de los repositorios deshabilitados.

Para habilitar un repositorio, usamos la opción `--enablerepo`, seguido del «repo id» de la lista. Esto sólo funciona en conexión con un comando `yum`; la repolist permanece intacta:

	$ yum --enablerepo=rawhide repolist

### Instalar y desinstalar paquetes usando YUM

Para instalar un nuevo paquete usando YUM, debemos conocer su nombre. YUM comprueba en los repositorios activos un nombre similar, resuelve las dependencias, descarga el paquete, y los que sean necesarios, y por último, los instala.

	# yum install hello
	# yum install openssh

Eliminar paquetes es así de simple:

	# yum remove hello

esta opción también desinstala los paquetes de los que depende, y no sean requeridos por otros paquetes.

Podemos actualizar paquetes:

	# yum update hello

para actualizar todos los paquetes:	

	# yum update

### Información sobre paquetes

El comando `yum list` está disponible para saber si un paquete existe:

	$ yum list gcc
	$ yum list “gcc*”

los «Installed Packages» están disponibles en el sistema local, mientras que los «Available Packages» pueden ser localizados en los repositorios.

	$ yum list installed “gcc*”
	$ yum list available “gcc*”

El comando:

	$ yum list updates

muestra todos los paquetes instalados para los cuales existe una actualización disponible en los repositorios.

	$ yum list recent

indica los paquetes que se han incorporado recientemente al repositorio.

	$ yum info hello

muestra información adicional sobre un paquete. A diferencia de «rpm -qi», también muestra información de paquetes no instalados, pero disponibles en los repositorios.

	$ yum search mysql

busca todos los paquetes, en cuyo nombre o descripción se encuentra la cadena indicada. Para examinar las dependencias de un paquete:

	$ yum deplist gcc

### Descarga de paquetes

Si lo que queremos es descargar un paquete de un repositorio, pero sin instalarlo, podemos usar el programa `yumdownloader`.

	$ yumdownloader --destdir /tmp hello

busca en los repositorios el paquete `hello`, y lo descarga en el directorio `/tmp`.

La opción `--resolve` descargaría también las dependencias necesarias, y que no estuvieran presentes en el equipo. Y la opción `--source`, descarga las fuentes en lugar de los binarios.

	$ yumdownloader --source --destdir /tmp hello

## Comandos vistos en el tema

Comando | Descripción
--| --
cpio | Gestor de archivos
rpm | Herramienta de gestión de paquetes usada por varias distribuciones Linux.
rpm2cpio | Convierte paquetes RPM a archivos cpio.
yum | Potente herramienta de gestión de paquetes.
