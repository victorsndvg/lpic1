#!/bin/sh

for f in *			# Recorremos la lista formada por todos los ficheros
				# del directorio actual (*).
do
	mv "$f" "$f.txt"	# Renombramos todos los ficheros, añadiendoles la
				# extensión ".txt"
done
