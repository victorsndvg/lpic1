#!/bin/bash
# Comprueba la existencia de los ficheros pasados como argumentos,
# cuando llega a un elemento de la lista que no existe como fichero, sale del bucle,
# finalizando la ejecución del script.

# Creamos dos ficheros  "touch a b"
# y comprobamos el funcionamiento del script: "breakdemo a c b"
# el script nos debe devolver la cadena "a", ya que al llegar al segundo argumento
# "c", y no existir como fichero se ejecuta la ruptura del bucle.


for f				# Recorremos la lista formada por todos los argumentos
do
	[ -f $f ] || break	# Realizamos un test "[]" para comprobar si $f existe
				# y es un fichero regular (-f), y en caso de que el
				# resultado sea negativo ejecutamos el comando 'break'.
	echo $f
done
