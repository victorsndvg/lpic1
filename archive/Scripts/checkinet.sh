#!/bin/bash

device="$1"
#	Si vols fer-ho per a tots els dispositius, canvia la lin
#	anterior pel bucle for q ve ara :D

#for device in $(ifconfig | grep "Link encap:" |  awk '{print $1}' | grep -v "lo");
#do
	echo "Checking $device"

	tinc_interface=$(ifconfig $device 2>&1 | grep -c "Device not found" )

	if [ "$tinc_interface" -ne "0" ]; then

		echo "No tinc tarja de xarxa -$device-!!!!!"
		exit

	fi

	tinc_link=$(ethtool $device | grep -c "Link detected: yes")

	if [ "$tinc_link" -eq "0" ]; then

		echo "No tinc link a $device!!!!!"
		exit

	fi

	tinc_ip=$(ip route | grep -c $device)

	if [ "$tinc_ip" -eq "0" ]; then

		echo "No tinc cap IP a $device!!!!!"
		exit
	fi

	
	my_gateway=$(ip route | grep "^default" | awk '/'$device'/ {print $3}')
	
	echo "Gateway = $my_gateway"
	if [ ! -n "$my_gateway" ]; then

		echo "No tinc cap Gateway a $device!!!!!"
		exit
	fi


	error_ping_gateway=$(ping -c 3 $my_gateway 2>&1 | grep -c "100% packet loss" )

	if [ "$error_ping_gateway" -ne "0" ]; then

		echo "No tinc connectivitat amb el gateway: $my_gateway!!!!!"
		exit
	fi


	for nameserver in $(grep "^nameserver" /etc/resolv.conf | awk '{print $2}'); do

		error_resolucio=$(host www.google.com $nameserver | grep -c "has address")

		if [ "$error_resolucio" -eq "0" ]; then

			echo "El servidor de noms -$nameserver- no xuta!!!!!"
			exit
		fi	


	done

	falla_internet=$(hping3 -c 3 -S -p 80 74.125.43.147 2>&1 | grep -c "100% packet loss")

	if [ "$falla_internet" -eq "1" ]; then

		echo "A alguna banda ens han tallat internet......."
		exit

	fi

	echo "Tot funciona correctament"
