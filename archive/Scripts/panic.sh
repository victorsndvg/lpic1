#!/bin/bash

function panic () {
	exitcode=$1
	shift
	echo >&2 "$0: PANIC: $*"
	exit $exitcode
}

[ -f file.txt ] || panic 3 O ficheiro file.txt non existe.
