#!/bin/bash
#tclock - Muestra un reloj en la terminal de texto. Se debe finalizar la ejecución
# pulsando la combinación CTRL+C, y así enviarle una señal INT, que el script atrapará.


trap "clear; exit" INT			# Si se recibe la señal de interrupción INT
					# ejecutarse el comando "clear; exit".

while true				# Bucle infinito
do
	clear
	banner -w 40 $(date +%X)	# Se ajusta la salida del comando banner a un
					# de 40, y se visualiza la hora actual.
	sleep 1
done
