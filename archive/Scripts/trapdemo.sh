#!/bin/sh
#Nombre: trapdemo. Ejecutaremos el script en segundo plano, y luego le enviamos una
# señal TERM con el comando kill (kill -TERM %x), donde x es el número de trabajo del
# script. Fijarse en el valor devuelto al finalizar la ejecución del script (143), que
# resulta de 128+15.

trap "echo Señal recibida" TERM HUP	# Cuando se reciban las señales TERM o HUP se
					# ejecutará el comando "echo Señal recibida"
sleep 60
