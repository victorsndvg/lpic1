#!/bin/bash
# Este script realiza la búsqueda de un patrón en una lista de ficheros, y aquellos
# ficheros que contengan dicho patrón, son copiados al directorio backups dentro del
# directorio $HOME.
#
# Uso:	continuedemo <patron> <fichero 1>...<fichero n>
#

patron=$1			# Almacenamos en la variable patron el argumento
				# numero 1, que contiene el patrón a buscar.

shift				# Desplazamos las posiciones de los argumentos hacia
				# la izquierda, eliminando así el parámetro que
				# contenía el patron buscar, quedandonos con la
				# lista de ficheros en los cuales realizar la
				# búsqueda.

for f				# Recorremos la lista de argumentos, formada ahora
				# por los nombres de los ficheros únicamente.

do
	fgrep -q $patron $f	# El comando fgrep busca en el fichero $f las lineas
				# que contengan el patrón $patron, y nos devuelve
				# las lineas que contienen dicho patron. Pero con la
				# opción "-q", hacemos que funcione en modo
				# silencioso sin enviar resultado a su salida
				# estándar
	if [ $? = 1 ]		# Si el código de retorno del comando anterior es 1
				# (error) debemos continuar con	la siguiente
				# iteración del bucle, es decir, con el siguiente
				# fichero.
	then
		continue
	fi

	cp $f $HOME/backups	# Si se ha llegado hasta esta línea es porque se ha
				# localizado un fichero con ese	patron y no se ha
				# interrumpido la ejecución de esta iteración, y por
				# lo tanto, realizamos la copia de dicho fichero.
done
