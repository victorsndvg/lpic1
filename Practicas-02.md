# Prácticas
## Gestión de ficheros

1. A partir del directorio personal, crear la siguiente estructura **utilizando un único comando**:

	```
	|---carpeta1
	|   |---carpeta3
	|---carpeta2
	|   |---carpeta4
	```

2. Desde el directorio `carpeta1`, crear un fichero con el nombre `fichero1` en `carpeta3`. A continuación, moverse a la `carpeta2` haciendo uso de una ruta relativa, y copiar el fichero `fichero1` de `carpeta3` al directorio actual con el nombre `fichero2`. Escribir los comandos necesarios para ejecutar las anteriores acciones.
3. ¿Cuál es el objetivo del comando `ls -l [a-z]*.??[!0-9]`?
4. Buscar todos los ficheros del sistema que tengan un tamaño inferior a 400 bytes y con permisos 644. Redirigir la salida de error estándar a `/dev/null`.
5. Listar todos los ficheros de tu directorio personal, y que sean de tu propiedad, que hayan sido modificados hace más de 7 días.
6. Visualizar en formato extendido (long) los ficheros del directorio `/etc` que tengan un tamaño entre 512 y 1024 bytes, ambas cantidades incluídas.
7. Localizar todos los ficheros vacíos de nuestro sistema, e intentar eliminarlos con confirmación.
8. Crea un directorio temporal en `/tmp/files`, y copia ahí todos los ficheros del directorio `/usr/share` mayores de 3MB y menores de 10MB.
9. Crear una copia de seguridad de cada uno de los ficheros del directorio `/tmp/files` añadiendo la extensión `.backup` a su nombre actual.
