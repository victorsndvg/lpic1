# lpic1
Temario para preparación de la certificación LPIC1

Contenidos publicados en formato Markdown bajo una licencia Apache 2.0

* [Tema 1 - Introducción a GNU/Linux](Tema01-introduccion-gnulinux.md)
* [Tema 2 - Usando Linux](Tema02-usando-linux.md)
* [Tema 3 - ¿Quién teme a la Shell?](Tema03-quien-teme-a-la-shell.md)
* [Tema 6 - Ficheros](Tema06-ficheros.md)
* [Tema 8 - Comandos de filtrado - Entrada y Salida](Tema08-comandos-filtrado-entrada-salida.md)
* [Tema 9 - El sistema de ficheros](Tema09-sistema-de-ficheros.md)
* [Tema 11 - Gestión de procesos](Tema11-gestion-de-procesos.md)
* [Tema 12 - Particiones y Sistemas de ficheros](Tema12-particiones-sistemas-ficheros.md)
* [Tema 16 - Librerías compartidas](Tema16-librerias-compartidas.md)
* [Tema 17 - Gestión de paquetes en Debian](Tema17-gestion-paquetes-debian.md)
* [Tema 18 - Gestión de paquetes con RPM y YUM](Tema18-gestion-paquetes-rpm.md)
* [Tema 30 - Shells](Tema30-shells.md)
* [Tema 31 - Scripts de Shell](Tema31-scripts-shell.md)
* [Tema 32 - La Shell como lenguaje de programación](Tema32-shell-lenguaje-de-programacion.md)
* [Tema 34 - Automatizar tareas](Tema34-automatizar-tareas.md)
* [Tema 39 - Archivado de ficheros](Tema39-archivado-ficheros.md)
* [Tema 40 - Scripts interactivos](Tema40-scripts-interactivos.md)
* [Tema 41 - Systemd](Tema41-systemd.md)
