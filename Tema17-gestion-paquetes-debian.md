# Tema 17 - Gestión de paquetes en Debian

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Objetivos](#objetivos)
- [Herramientas `dpkg`](#herramientas-dpkg)
- [Paquetes Debian](#paquetes-debian)
    - [Instalación de paquetes](#instalación-de-paquetes)
    - [Problemas durante la instalación](#problemas-durante-la-instalación)
    - [Ejercicios 1](#ejercicios-1)
- [Eliminado de paquetes](#eliminado-de-paquetes)
    - [Ejercicios 2](#ejercicios-2)
- [Paquetes Debian y código fuente](#paquetes-debian-y-código-fuente)
    - [Ejercicios 3](#ejercicios-3)
- [Información de paquetes](#información-de-paquetes)
    - [Ejercicios 4](#ejercicios-4)
- [Gestión de paquetes Debian. APT.](#gestión-de-paquetes-debian-apt)
    - [Ejercicios 5](#ejercicios-5)
- [Búsqueda de paquetes](#búsqueda-de-paquetes)
- [APTITUDE](#aptitude)
- [La infraestructura `debconf`](#la-infraestructura-debconf)
- [ALIEN](#alien)
- [Comandos vistos en el tema](#comandos-vistos-en-el-tema)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivos

* Conocer las herramientas básicas de paquetes Debian.
* Ser capaces de usar `dpkg` para la gestión de paquetes.
* Ser capaces de usar `apt-get`, `apt-cache` y `aptitude`.
* Saber convertir paquetes RPM a paquetes Debian.

## Herramientas `dpkg`

Funciones del comando `dpkg`:

* Instalar paquetes de software
* Gestionar dependencias
* Catalogar el software instalado
* Controlar las actualizaciones de los paquetes
* Desinstalar paquetes

`aptitude` es un «front-end» de `dpkg`.
`debconf` configura los paquetes bajo una instalación.

## Paquetes Debian

El software en Debian se divide en paquetes, que tienen nombres indicando el software que contienen dentro.

	figlet_2.2.5-3_amd64.deb

El programa `figlet`, en su versión 2.2.5, en particular la liberación 3 de este paquete en la distribución (la versión 2.2.6 comenzará de nuevo en 1). El *amd64* indica que el paquete contiene partes específicas de la arquitectura *amd64*. Los paquetes que sólo contienen documentación o que son scripts independientes de la arquitectura usan *all* en su lugar.

> Un paquete Debian es un archivo creado con el programa `ar`, y contiene tres componentes:<br/>
`$ ar t figlet_2.2.5-3_amd64.deb`<br/>
  `debian-binary`<br/>
  `control.tar.gz`<br/>
  `data.tar.gz`<br/>
> El fichero `debian-binary` contiene el número de versión del paquete. En `control.tar.gz` están los scripts específicos para Debian, y ficheros de configuración. Y en `data.tar.gz` están los ficheros del paquete. Durante la instalación, el fichero `control.tar.gz` se desempaqueta, por lo que se ejecutan los scripts `preinst`. Luego se desempaqueta el fichero `data.tar.gz`, y luego los scripts de `postinst`.

	$ ar x figlet_2.2.5-3_amd64.deb
	$ tar tf control.tar.gz
	$ tar tf data.tar.gz
	$ cat debian-binary

### Instalación de paquetes

Podemos instalar un paquete Debian disponible localmente usando `dpkg`:

	# dpkg --install figlet_2.2.5-3_amd64.deb

donde `--install` puede ser abreviado con `-i`. Con las opciones `--unpack` y `--configure`, se pueden ejecutar de forma separada.

> Las opciones para `dpkg` se pueden pasar a través de la línea de comadnos, o bien situarlas en `/etc/dpkg/dpkg.cfg`. En este fichero, se omiten los guiones al principio de los nombres de las opciones.

Cuando hacemos una instalación, y existe una versión anterior, es desinstalada antes de configurar la nueva. Si hay un error durante la instalación, la versión anterior puede ser restaurada.

### Problemas durante la instalación

1. Son necesarios uno o más paquetes (`--force-depends`). El paquete necesita uno o más paquetes que no han sido instalados, o que no han sido incluidos en la misma instalación.
2. Existe una versión anterior (`hold`). Hay una versión anterior instalada y marcada como «hold» (usando `aptitude`). Esto impide la instalación de nuevas versiones.
3. El fichero desempaquetado ya existe en otro paquete diferente (`--force-overwrite`). El paquete intenta desempaquetar un fichero que ya existe y pertenece a un paquete diferente. A menos que el paquete a instalar esté marcado como reemplazo del paquete existente, o usemos la opción `--force-overwrite`.
4. Algunos paquetes entran en conflicto con otros. Por ejemplo, sólo podemos tener instalado un paquete para el transporte del correo. Si queremos instalar `Postfix`, debemos desinstalar `Exim`.

### Ejercicios 1

1. Descargar el paquete `hello` e instalarlo usando la herramienta `dpkg`.
2. Localiza la lista actual de los paquetes virtuales en tu distribución.

## Eliminado de paquetes

Un paquete se desinstala usando:

	# dpkg --remove figlet

Los ficheros de configuración (aquellos listados en el fichero `conffiles`, en `control.tar.gz`), se mantienen para una próxima reinstalación.

	# dpkg --purge figlet

elimina el paquete incluyendo sus ficheros de configuración.

> Examinar el fichero `/var/lib/dpkg/info/<nombre_paquete>.conffiles`

Posibles problemas en la desinstalación:

* El paquete sea requerido por uno o más paquetes que todavía están instalados.
* El paquete tiene la marca de «essential». Por ejemplo, no podemos eliminar la shell.

### Ejercicios 2

1. Desinstala el paquete `figlet`. Utiliza la opción adecuada para que se eliminen también los ficheros de configuración.
2. Desinstala el resto de paquetes instalados.

## Paquetes Debian y código fuente

Cuando trabajamos con código fuente, un principio básico de Debian es distinguir claramente entre el código original y cualquier cambio específico de Debian. Todos los cambios son colocados en un fichero que se crea usando el comando `diff`, y que contiene las diferencias entre la versión original y la versión Debian. Para cada versión del paquete, también existe un «fichero de control de la fuente» (con sufijo `.dsc`) que contiene los «checksums» del fichero original y el fichero de cambios, que será firmado digitalmente por el responsable de Debian a cargo del paquete:

	figlet_2.2.5-3.dsc
	figlet_2.2.5.orig.tar.gz
	figlet_2.2.5-3.debian.tar.gz

El comando ´dpkg-source´ se usa para reconstruir el código fuente de un paquete desde los archivos originales y los cambios de Debian, de tal forma que podemos recompilar nuestra propia versión del paquete Debian. Para realizar esto, debemos ejecutar este comando con el fichero de control de la fuente como argumento:

	$ dpkg-source -x figlet_2.2.5-3.dsc

el fichero original y el fichero `diff.gz` debe estar en el mismo directorio que el fichero de control de la fuente.

### Ejercicios 3

1. Obtener el código fuente del paquete *figlet*, y desempaquetarlo. Observar el directorio resultado.

## Información de paquetes

Podemos obtener una lista de los paquetes instalados usando:

	$ dpkg --list

esta lista la podemos filtrar usando los patrones de búsqueda de la shell:

	$ dpkg -l libc*

Los paquetes con version *<ninguna>*, son parte de la distribución pero todavía no están instalados en el sistema (estado `un`), o han sido eliminados (estado `pn`). Podemos encontrar información acerca de un paquete concreto con la opción `--status`:

	$ dpkg --status figlet

Tras el nombre del paquete, la salida muestra información acerca del estado en el que se encuentra el paquete, su prioridad, el nombre de la persona a cargo del paquete en el proyecto Debian (*Maintainer*), y un área importante de información como son las dependencias, recomendaciones o conflictos con otros paquetes. Si el paquete no está instalado localmente, sólo se muestra un mensaje de error.

	# dpkg -s apache

La opción `--listfiles` (-L), muestra una lista de los ficheros dentro del paquete:

	$ dpkg --listfiles figlet
	$ dpkg -L coreutils

Por último, con la opción `--search` (-S) podemos encontrar el paquete al que pertenece un determinado fichero. Están permitidos los patrones de búsqueda:

	$ dpkg -S bin/m*fs
	$ dpkg -S /usr/bin/basename

### Ejercicios 4

1. ¿Cuántos paquetes hay instalados en el sistema cuyo nombre comience por «lib»?

## Gestión de paquetes Debian. APT.

`dpkg` es una potente herramienta, pero tiene limitaciones, por ejemplo, que no sea capaz de resolver las dependencias entre los paquetes. Es válida para realizar instalaciones de paquetes que están disponibles de forma local, pero no permite el acceso a servidores web o FTP. La herramienta `apt-get` representa una versión inteligente de `dpkg`. No ofrece una interfaz interactiva para la selección de paquetes, pero hoy en día es la más utilizada en la línea de comandos.

**Propiedades de apt-get**

1. Manejo de varias fuentes.
2. Actualización de la distribución (`dist-upgrade`)
3. Herramientas auxiliares (`apt-proxy`, `apt-zip`, `apt-listbugs`...)

Las fuentes de paquetes para `apt-get` están listadas en el fichero `/etc/apt/sources.list`:

	deb http://ftp.de.debian.org/debian/ stable main
	deb-src http://ftp.de.debian.org/debian/ stable main

La ejecución estándar del comando `apt-get` comienza con la actualización de la base de datos de paquetes disponibles:

	# apt-get update

Esto consulta todas las fuentes de paquetes e integra los resultados en una lista común. Podemos instalar paquetes usando:

	# apt-get install figlet

De esta forma instalaremos también los paquetes listados en *Depends*, así como cualquier paquete del que dependan éstos a su vez.

El comando `apt-get upgrade` instala las nuevas versiones disponibles de todos los paquetes instalados en el sistema. Aquellos paquetes que no dispongan de una nueva versión, permanecen intactos. El comando `apt-get dist-upgrade` ejecuta un esquema «inteligente» de resolución de conflictos que intenta resolver las dependencias de forma prudente eliminando e instalando paquetes, dando prioridad a los más importantes. Podemos obtener el código fuente de un paquete con el comando:

	# apt-get source figlet

### Ejercicios 5

1. Usa el comando `apt-get` para instalar el paquete *hello*, y luego eliminalo de nuevo.
2. Descarga el código fuente para el paquete *hello* usando `apt-get`.

## Búsqueda de paquetes

Otra utilidad de `apt-get` es el programa `apt-cache`, que busca en las fuentes de `apt-get`:

	$ apt-cache search figlet
	$ apt-cache show figlet

La salida de `apt-cache show` corresponde a la de `dpkg --status`, excepto que en este caso funciona con todos los paquetes presentes en alguna de las fuentes, aunque no esté instalado localmente. Hay otros usos interesantes de `apt-cache`:

	$ apt-cache depends figlet
	$ apt-cache rdepends libxpm4
	$ apt-cache stats

## APTITUDE

El programa `aptitude` realiza funciones de selección de paquetes y gestión de los mismos. Presenta ciertas mejoras respecto a `dpkg` y `apt-get`:

**Mejoras de aptitude**

1. No es necesario ser *root*.
2. Recuerda los paquetes que han sido instalados.
3. Acceso a todas las versiones en varias fuentes.

## La infraestructura `debconf`

En ocasiones, es necesario responder a algunas cuestiones durante la instalación de paquetes. Por ejemplo, si estamos instalando un servidor de correo, es importante conocer datos acerca de la configuración de red del equipo en el cual se está instalando el servidor. El mecanismo *debconf* está diseñado para recoger esta información y almacenarla para un uso futuro. Por lo tanto, si necesitamos reconfigurar la información almacenada por *debconf*, debemos utilizar:

	# dpkg-reconfigure my-package

de esta forma se repiten las cuestiones realizadas durante la instalación.

	# dpkg-reconfigure locales

## ALIEN

Mucho software sólo está disponible para el formato *RPM*. Por ejemplo, hay software comercial que se oferta para Red Hat o SUSE. En Debian no podemos instalar los paquetes *rpm*, pero el programa `alien` hace posible la conversión entre varios formatos de distribuciones Linux al formato de paquetes de Debian. Es importante entender que la conversión de paquetes de un formato a otro, puede dar como resultado un paquete que no se pueda utilizar de forma correcta. Aparte de la posible existencia de librerías que no están disponibles. El uso de este programa se realiza del siguiente modo:

	# alien --to-deb paket.rpm
	# alien --to-rpm paket.deb

## Comandos vistos en el tema

Comando | Descripción
-- | --
alien | Convierte varios formatos de paquetes software.
apt-get | Potente herramienta de línea de comandos para la gestión de paquetes Debian GNU/Linux
aptitude | Herramienta de instalación y mantenimiento de paquetes.
dpkg | Herramienta de gestión de paquetes Debian GNU/Linux
dpkg-reconfigure | Reconfigura un paquete ya instalado.
